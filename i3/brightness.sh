#!/usr/bin/env bash

output="eDP-1"
max_brightness=90
min_brightness=40
step=3

brightness=$(xrandr --verbose | grep Brightness | awk '{print $2 * 100}')

if [[ ${1} = "inc" ]]; then
    new=$((brightness + step))
    if [ $new -gt $max_brightness ]; then
        new=$max_brightness
    fi
else
    new=$((brightness - step))
    if [ $new -lt $min_brightness ]; then
        new=$min_brightness
    fi
fi

new=$(echo "scale=2; $new / 100" | bc)
xrandr --output ${output} --brightness $new
